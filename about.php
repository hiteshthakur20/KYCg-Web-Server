<?php
// Start the session
session_start();
?>
<!DOCTYPE html>
<html lang="">
<head>
<title>About</title>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
<link href="layout/styles/layout.css" rel="stylesheet" type="text/css" media="all">
<!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">

<!-- jQuery library -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>

<!-- Latest compiled JavaScript -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

<link href='https://fonts.googleapis.com/css?family=Aldrich' rel='stylesheet'>

  <link rel="icon" type="image/png" href="contactform/images/icons/favicon.ico"/>    
  <link rel="stylesheet" type="text/css" href="contactform/css/main.css">
    </head>
<body id="top">
<div class="wrapper row0" style="background-color: #003300">
  <div id="topbar" class="hoc clear"> 
    <div class="fl_left">
      <ul class="nospace">
        <li><a href="index.php"><i class="fa fa-lg fa-home"></i></a></li>
        <li><a href="tutorial.php" style="color: #ffffff">Tutorial</a></li>
        <li><a href="about.php" style="color: #ffffff">About</a></li>
        <li><a href="contact.php" style="color: #ffffff">Contact</a></li>
      </ul>
    </div>
    <div class="fl_right">
      <ul class="nospace">
        <li style="color: #ffffff"><i class="fa fa-phone"></i> +91-8629857986</li>
        <li style="color: #ffffff"><i class="fa fa-envelope-o"></i> hiteshthakur20@gmail.com</li>
      </ul>
    </div>
  </div>
</div>
  <nav class="navbar" style="margin-top: 19px; border-radius: 0px;">
  <div class="container-fluid">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle" style="border-color: red" data-toggle="collapse" data-target="#myNavbar">
        <span class="icon-bar" style="background-color: red"></span>
        <span class="icon-bar" style="background-color: red"></span>
        <span class="icon-bar" style="background-color: red"></span> 
      </button>
      <a class="navbar-brand" href="#" style="font-family: 'Aldrich';font-size: 46px; margin-left:15%;">KYCg</a>
    </div>
    <div class="collapse navbar-collapse" id="myNavbar">
      <ul class="nav navbar-nav navbar-right" style="font-size: 16px; margin-right:10%;">
        <li class="active"><a href="index.php">Home</a></li>
        <li><a href="tutorial.php">Tutorial</a></li>
        <li><a href="about.php">About</a></li>
        <li><a href="contact.php">Contact</a></li> 
      </ul>
    </div>
  </div>
</nav>
<div style="background-image: url('aboutbg.jpg');">
<div class="container" style="padding-top: 0px; padding-bottom: 0px;">
<div class="row" >
	<div class="col-md-12" style="text-align: center;font-size: 250%;color: #343232;">MEET THE TEAM</div>
	<hr style="border-width: 2px;border-color: #343232;">
</div>
</div>
<br>
<div class="container" style="padding-top: 0px; padding-bottom: 0px;">
<div class="row">
  <!--<div class="col-md-4" align="center"><img src="" style="max-width: 300px; max-height: 300px;"></div>-->
  <div class="col-md-12" align="center"><p style="font-size: 20px; font-weight: bold">Hitesh Thakur<p><p style="font-size: 16px;font-family: arial;font-style: italic;"> B.Tech Bioinformatics, Intern visiting from JUIT SOLAN H.P</p>
  <ul class="faico clear">
          <li><a class="faicon-facebook" href="#"><i class="fa fa-facebook"></i></a></li>
          <li><a class="faicon-google" href="#"><i class="fa fa-dribbble"></i></a></li>
          <li><a class="faicon-linkedin" href="#"><i class="fa fa-linkedin"></i></a></li>
        </ul>
  </div>

</div>
</div>
<br>
<div class="container" style="padding-top: 0px; padding-bottom: 0px;">
<div class="row">
  <!--<div class="col-md-4" align="center"><img src="" style="max-width: 300px; max-height: 300px;"></div>-->
  <div class="col-md-12" align="center"><p style="font-size: 20px; font-weight: bold">Prasad Gandham<p><p style="font-size: 16px;font-family: arial;font-style: italic;"> Bioinformatics Researcher, SBDM unit ICRISAT</p> 
  <ul class="faico clear">
          <li><a class="faicon-facebook" href="#"><i class="fa fa-facebook"></i></a></li>
          <li><a class="faicon-google" href="#"><i class="fa fa-dribbble"></i></a></li>
          <li><a class="faicon-linkedin" href="#"><i class="fa fa-linkedin"></i></a></li>
        </ul>
  </div>
</div>
</div>
<br>
<div class="container" style="padding-top: 0px; padding-bottom: 0px;">
<div class="row">
  <!--<div class="col-md-4" align="center"><img src="" style="max-width: 300px; max-height: 300px;"></div>-->
  <div class="col-md-12" align="center"><p style="font-size: 20px; font-weight: bold">Pulkit Anupam Srivastava<p><p style="font-size: 16px;font-family: arial;font-style: italic;"> B.Tech Bioinformatics, Intern visiting from JUIT SOLAN H.P</p>
  <ul class="faico clear">
          <li><a class="faicon-facebook" href="#"><i class="fa fa-facebook"></i></a></li>
          <li><a class="faicon-google" href="#"><i class="fa fa-dribbble"></i></a></li>
          <li><a class="faicon-linkedin" href="#"><i class="fa fa-linkedin"></i></a></li>
        </ul>
  </div>
</div>
</div>
<br>
<div class="container" style="padding-top: 0px; padding-bottom: 0px;">
<div class="row">
  <!--<div class="col-md-4" align="center"><img src="" style="max-width: 300px; max-height: 300px;"></div>-->
	<div class="col-md-12" align="center"><p style="font-size: 20px; font-weight: bold">Vivek Thakur</p><p style="font-size: 16px;font-family: arial;font-style: italic;"> Bioinformatics Researcher, SBDM Unit ICRISAT</p>
  <ul class="faico clear">
          <li><a class="faicon-facebook" href="#"><i class="fa fa-facebook"></i></a></li>
          <li><a class="faicon-google" href="#"><i class="fa fa-dribbble"></i></a></li>
          <li><a class="faicon-linkedin" href="#"><i class="fa fa-linkedin"></i></a></li>
        </ul>
	</div>
</div>
</div>
<br>
<div class="container" style="padding-top: 0px; padding-bottom: 0px;">
<div class="row">
  <!--<div class="col-md-4" align="center"><img src="" style="max-width: 300px; max-height: 300px;"></div>-->
  <div class="col-md-12" align="center"><p style="font-size: 20px; font-weight: bold">Abishek Rathore<p><p style="font-size: 16px;font-family: arial;font-style: italic;"> Theme Leader and Principal Scientist, SBDM Unit ICRISAT</p>
  <ul class="faico clear">
          <li><a class="faicon-facebook" href="#"><i class="fa fa-facebook"></i></a></li>
          <li><a class="faicon-google" href="#"><i class="fa fa-dribbble"></i></a></li>
          <li><a class="faicon-linkedin" href="#"><i class="fa fa-linkedin"></i></a></li>
        </ul>
  </div>
</div>
</div>
</div>
<br>
<div style="background-color: #003300"> 
  <div class="wrapper row4 row">
    <footer id="footer" class="hoc clear"> 
      <div class="col-md-6" style="margin-top: 20px">
        <h6 class="heading" style="font-family: 'Aldrich';">Contact Us</h6>
        <ul class="nospace btmspace-30 linklist contact">
          <li><i class="fa fa-map-marker"></i>
            <address>
            Jaypee University of Information Technology, Solan
            </address>
          </li>
          <li><i class="fa fa-phone"></i> +91-8629857986</li>
          <li><i class="fa fa-envelope-o"></i> hiteshthakur20@gmail.com</li>
        </ul>
        <ul class="faico clear">
          <li><a class="faicon-facebook" href="#"><i class="fa fa-facebook"></i></a></li>
          <li><a class="faicon-twitter" href="#"><i class="fa fa-twitter"></i></a></li>
          <li><a class="faicon-dribble" href="#"><i class="fa fa-dribbble"></i></a></li>
          <li><a class="faicon-linkedin" href="#"><i class="fa fa-linkedin"></i></a></li>
        </ul>
      </div>
      <div class="col-md-6" style="margin-top: 20px">
        <h6 class="heading" style="font-family: 'Aldrich';">Know Your Candidate-gene</h6>
        <p class="nospace btmspace-15" style="color: #FFFFFF;">Enable users more information about the function or biological role of the gene, which should help them in finding causal gene(s).</p>
        <form method="post" action="#">
          <fieldset>
            <p style="color: #FFFFFF;font-size: 20px;">Send Us A Mail :</p>
            <input class="btmspace-15" type="text" value="" placeholder="Name">
            <input class="btmspace-15" type="text" value="" placeholder="Email">
            <button type="submit" value="submit">Submit</button>
          </fieldset>
        </form>
      </div>
    </footer>
  </div>
  <div class="wrapper row5">
    <div id="copyright" class="hoc clear"> 
      <p class="fl_left">Copyright &copy; 2018 - All Rights Reserved - <a href="#">ICRISAT</a></p>
      <p class="fl_right">Published by <a target="_blank">SDBM Unit, ICRISAT</a></p>
    </div>
  </div>
</div>
</body>
</html>