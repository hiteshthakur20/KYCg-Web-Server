# KYCg-Web-Server
Web server to predict the function of the uncharacterized genes.
This webserver requires linux based system as a server to be fully functional.

Home page:

![](ImagesforReadme/img1.jpg)

Result page:

![](ImagesforReadme/img2.jpg)

![](ImagesforReadme/img3.jpg)

![](ImagesforReadme/img4.jpg)

![](ImagesforReadme/img5.jpg)

![](ImagesforReadme/img6.jpg)

![](ImagesforReadme/img7.jpg)

![](ImagesforReadme/img8.jpg)

Tutorial:

![](ImagesforReadme/img9.png)

Contact:

![](ImagesforReadme/img10.png)
